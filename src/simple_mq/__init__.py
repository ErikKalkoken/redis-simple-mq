"""Simple message queue based on Redis."""

from .core import SimpleMQ

__all__ = ["SimpleMQ"]
__version__ = "1.0.0"
