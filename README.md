# redis-simple-mq

A simple message queue for Redis.

[![release](https://img.shields.io/pypi/v/redis-simple-mq?label=release)](https://pypi.org/project/redis-simple-mq/)
[![python](https://img.shields.io/pypi/pyversions/redis-simple-mq)](https://pypi.org/project/redis-simple-mq/)
[![pipeline](https://gitlab.com/ErikKalkoken/redis-simple-mq/badges/master/pipeline.svg)](https://gitlab.com/ErikKalkoken/redis-simple-mq/-/pipelines)
[![codecov](https://codecov.io/gl/ErikKalkoken/redis-simple-mq/branch/master/graph/badge.svg?token=M1IBQV97BE)](https://codecov.io/gl/ErikKalkoken/redis-simple-mq)
[![Documentation Status](https://readthedocs.org/projects/redis-simple-mq/badge/?version=latest)](https://redis-simple-mq.readthedocs.io/en/latest/?badge=latest)
[![license](https://img.shields.io/badge/license-MIT-green)](https://gitlab.com/ErikKalkoken/redis-simple-mq/-/blob/master/LICENSE)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![chat](https://img.shields.io/discord/790364535294132234)](https://discord.gg/zmh52wnfvM)

## Description

This is a light-weight message queue based on Redis.

Key features:

- Class based API to the queue with all basic queue functions
- Queue is implement as FIFO
- All messages are stored and retrieved as UTF-8 strings
- Bulk methods for enqueue and dequeue
- No limit on the number of parallel queues
- Fully tested

## Basic example

```python
from redis import Redis
from simple_mq import SimpleMQ

conn = Redis()
q = SimpleMQ(conn)
q.enqueue('Hello, World!')
message = q.dequeue()
print(message)
```

See also the examples folder for examples.
