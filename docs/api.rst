.. currentmodule:: simple_mq

===============
API Reference
===============

.. automodule:: simple_mq
    :members:
    :inherited-members:
